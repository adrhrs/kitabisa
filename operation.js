var n1 = 0
var n2 = 1
var n3 = 0

var fib = [0, 1]

module.exports = {
    tambahOperasi: function(input) {
        console.log("\n")

        angka = input.split(",")
        result = parseFloat(angka[0]) + parseFloat(angka[1])

        return Math.round(result * 100) / 100
    },

    kaliOperasi: function(input) {

        console.log("\n")

        angka = input.split(",")
        result = parseFloat(angka[0]) * parseFloat(angka[1])

        return Math.round(result * 100) / 100

    },

    primaOperasi: function(input) {

        var res = []
        var x = 2

        while (res.length < input) {

            var stat = 0
            for (var y = x - 1; y > 1; y--) {

                var diff = (x / y)
                if (Number.isInteger(diff)) {
                    stat = 1
                }

            }

            if (stat == 0) {
                res.push(x)
            }

            x += 1

        }


        return res

    },

    resetFibonacci: function() {

        n1 = 0
        n2 = 1
        n3 = 0

        fib = [0, 1]

    },


    fibbonacciRec: function(c) {
        var result = [0, 1]
        // console.log(c)
        if (c > 2) {
            n3 = n1 + n2
            n1 = n2
            n2 = n3
            fib.push(n3)
            module.exports.fibbonacciRec(c - 1)

        } else if (c == 1) {
            return [0]
        }

        result = fib

        return result
    }
};