var assert = require('assert');
var ops = require('./operation');

describe('Method Penjumlahan', function() {
    
    it('Unit Testing 2 + 3 adalah 5', function() {
        assert.equal(ops.tambahOperasi('2,3'), 5);
    });
    it('Unit Testing 34.54 + 87.22 adalah 121.76', function() {
        assert.equal(ops.tambahOperasi('34.54,87.22'), 121.76);
    });
    it('Unit Testing -10 + -52 adalah -62', function() {
        assert.equal(ops.tambahOperasi('-10,-52'), -62);
    });
    it('Unit Testing -98 + 102 adalah 4', function() {
        assert.equal(ops.tambahOperasi('-98,102'), 4);
    });
    it('Unit Testing a + b adalah NaN ( Not a Number )', function() {
        assert.equal(isNaN(ops.tambahOperasi('a,b')), true);
    });
    
});

describe('Method Perkalian', function() {
    
    it('Unit Testing 2 * 3 adalah 6', function() {
        assert.equal(ops.kaliOperasi('2,3'), 6);
    });
    it('Unit Testing 0.2 * 0.5 adalah 0.1', function() {
        assert.equal(ops.kaliOperasi('0.2,0.5'), 0.1);
    });
    it('Unit Testing -10 * -52 adalah 520', function() {
        assert.equal(ops.kaliOperasi('-10,-52'), 520);
    });
    it('Unit Testing 123 * 0 adalah 0', function() {
        assert.equal(ops.kaliOperasi('123,0'), 0);
    });
    it('Unit Testing a * b adalah NaN ( Not a Number )', function() {
        assert.equal(isNaN(ops.kaliOperasi('a,b')), true);
    });
    
});

describe('Method N Prima \n', function() {
    
    it('5 Bilangan Prima Pertama adalah 2,3,5,7,11', function() {
        assert.equal(ops.primaOperasi('5').toString(), '2,3,5,7,11');
    });

    it('1 Bilangan Prima Pertama adalah 2', function() {
        assert.equal(ops.primaOperasi('1').toString(), '2');
    });

    it('0 Bilangan Prima Pertama adalah ""', function() {
        assert.equal(ops.primaOperasi('0').toString(), '');
    });

    it('100 Bilangan Prima Pertama adalah 2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541', function() {
        assert.equal(ops.primaOperasi('100').toString(), '2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541');
    });
    
    
});

describe('Method N Fibbonaci \n', function() {

    it('1 Bilangan Fibbonaci Pertama adalah 0', function() {
        assert.equal(ops.fibbonacciRec('1').toString(), '0');
        ops.resetFibonacci()
    });

    it('2 Bilangan Fibbonaci Pertama adalah 0,1', function() {
        assert.equal(ops.fibbonacciRec('2').toString(), '0,1');
        ops.resetFibonacci()
    });

    it('3 Bilangan Fibbonaci Pertama adalah 0,1,1', function() {
        assert.equal(ops.fibbonacciRec('3').toString(), '0,1,1');
        ops.resetFibonacci()
    });

    it('5 Bilangan Fibbonaci Pertama adalah 0,1,1,2,3', function() {
        assert.equal(ops.fibbonacciRec('5').toString(), '0,1,1,2,3');
        ops.resetFibonacci()
    });
    it('15 Bilangan Fibbonaci Pertama adalah 0,1,1,2,3,5,8,13,21,34,55,89,144,233,377', function() {
        assert.equal(ops.fibbonacciRec('15').toString(), '0,1,1,2,3,5,8,13,21,34,55,89,144,233,377');
        ops.resetFibonacci()
    });


    
});