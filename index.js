const readline = require('readline');
var assert = require('assert');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var ops = require('./operation');

function tambah() {
    rl.question('\nOperasi Penjumlahan, masukkan angka dengan format seperti contoh: 3,4 \n ( tekan "0" untuk kembali ke menu utama ) \n ----------------- \n input = ', (answer) => {

        if (answer == '0') {
            main()
        } else {
            var result = ops.tambahOperasi(answer)

            if (isNaN(result)) {
                console.log("!!!! Format tidak sesuai !!!!")
                tambah()

            } else {

                if (angka.length != 2) {
                    console.log("!!!! jumlah input hanya 2 angka !!!!")
                    tambah()
                } else {
                    console.log('Jumlah Penjumlahan adalah ' + result)
                    tambah()
                }

            }
        }
    })
}


function kali() {
    rl.question('\nOperasi Perkalian, masukkan angka dengan format seperti contoh: 5,2 \n ( tekan "0" untuk kembali ke menu utama ) \n ----------------- \n input = ', (answer) => {

        if (answer == '0') {
            main()
        } else {
            var result = ops.kaliOperasi(answer)

            if (isNaN(result)) {
                console.log("!!!! Format tidak sesuai !!!!")
                kali()

            } else {

                if (angka.length != 2) {
                    console.log("!!!! jumlah input hanya 2 angka !!!!")
                    kali()
                } else {
                    console.log('Jumlah Perkalian adalah ' + result)
                    kali()
                }

            }
        }
    })
}



function prime() {

    rl.question('\n n Bilangan Prima Pertama, masukkan n (angka) sesuai format seperti contoh : 4 \n ( tekan "0" untuk kembali ke menu utama ) \n ----------------- \n input = ', (answer) => {

        if (answer == '0') {
            main()
        } else {

            console.log('\n')

            if ((answer != parseInt(answer).toString()) | !Number.isInteger(parseInt(answer))) {
                console.log("!!!! Format tidak sesuai !!!!")
                prime()

            } else {

                var res = ops.primaOperasi(answer)
                console.log(answer + ' Bilangan Prima Pertama adalah ' + res.toString())
                prime()

            }


        }
    })

}



function fibbonacci() {

    rl.question('\n n Bilangan Fibbonacci Pertama, masukkan n (angka) sesuai format seperti contoh : 7 \n ( tekan "0" untuk kembali ke menu utama ) \n ----------------- \n input = ', (answer) => {

        if (answer == '0') {
            main()
        } else {

            console.log('\n')

            if ((answer != parseInt(answer).toString()) | !Number.isInteger(parseInt(answer))) {
                console.log("!!!! Format tidak sesuai !!!!")
                fibbonacci()

            } else {

                var fibx = ops.fibbonacciRec(answer)
                console.log(answer + ' Bilangan Fibbonacci Pertama adalah ' + fibx)

                ops.resetFibonacci()

                fibbonacci()

            }


        }
    })

}

function main() {

    rl.question('\nOperasi apa yang mau dilakukan ? \n 1. Penjumlahan \n 2. Perkalian \n 3. Prima \n 4. Fibbonacci \n ----------------- \n pilihan = ', (answer) => {

        if (answer == '1') {
            tambah()
        } else if (answer == '2') {
            kali()
        } else if (answer == '3') {
            prime()
        } else if (answer == '4') {
            fibbonacci()
        } else {
            console.log('\nMenu invalid')
            main()
        }


    });

}

main()